# Hide and seek Client 

<img src="picture.png">

## Team members

- Persi MANKITA
- Ayinde Louisin
- Sanvee Alid Cédric Dovi
- Idriss Felloussi
---

## Get the server part here: [persimankita/hide-seek-server](https://gitlab.com/persimankita/hide-seek-server)


## Description

Client base for the Hide and Seek project. It consists in displaying with Three.js all the information of the Hide and Seek game server.

This project integrates:

* [three.js](http://threejs.org/)
* [alpine.js](https://alpinejs.dev/)
* [bootstrap](https://getbootstrap.com/)
* [webpack](https://webpack.js.org/)

**Note:** an introduction to the Alpine.js library is also available [resources/alpinejs_introduction.pdf](resources/alpinejs_introduction.pdf).

## Installation

```
npm install
```

**Note:** [`nodejs`](https://nodejs.org/en/download/) (recommended LTS) is required.

## Build and run

**build:**
```
npx webpack
```
or custom command:
```
npm run build
```

**run:**
```
node app.js
```
or custom command:
```
npm run serve
```

## Features list

- [x] Installation...

- [ ] ToDo check the project tasks list and specify here the integrated features


**Note:** any improvement of the client and are welcomed.



## License

[MIT](LICENSE)

## Credits


* [maze](https://github.com/johansatge/three-maze)
