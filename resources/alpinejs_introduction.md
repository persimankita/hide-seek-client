---
marp: true
theme: default
paginate: true
style: |
  section {
    border: 3px solid #3e81a8;
    position: fixed !important;
    top: 0;
  }
footer: "**Université du Littoral Côte d'Opale** - Éric Ramat / Jérôme Buisine"
---


# Alpine JS 
JavaScript Tag Framework 

## Cours d'introduction


---
# Notions principales

`Alpine.js` est un framework web minimaliste. Il comporte les principes suivants :

- Tout se base sur la directive (attribut de balise) `x-data`. Dans la directive `x-data`, en JavaScript, vous déclarez un objet de données qu'Alpine va tracer. 

   ```html 
    <div x-data="{message: 'Hello'}"></div>
    ```

- Chaque propriété de cet objet `x-data` sera mise à la disposition des autres directives de cet élément HTML. En outre, lorsque l'une de ces propriétés change, tout ce qui repose sur elle change également.

**Remarque :** pour qu'un élément du DOM soit reconnu comme un _composant_ de données `Alpine`, la directive `x-data`, même vide est nécessaire.

---
# Notions principales

Découpler le code `html` du `JavaScript` apporte une meilleure maintenabilité. Pour cela, en utilisant les normes ES6, nous allons importer `Alpine` au sein d'un script `js`:

```javascript
import Alpine from 'alpinejs'
 
window.Alpine = Alpine

/**
 * All registered Alpine.js components
 */
document.addEventListener('alpine:init', () => { 
  ...
});
```

**Note :** l'événement associé au document permet de renseigner l'ensemble des composants `Alpine` souhaités. Ils seront ainsi reconnus au sein du DOM.

---

# La directive x-data

Attacher une donnée composée (composant) à un élément du DOM : 
```html
<div x-data="dropdown">
  ...
</div>
```

Depuis l'initialisation d'**Alpine** (JavaScript), gérer le comportement de ce composant :

```javascript
Alpine.data('dropdown', () => ({
  open: false,
 
  toggle() { 
    this.open = ! this.open
  }
}))
```

---
# La directive x-on

Attacher un événement à un élément du DOM : 
```html
<div x-data="dropdown" x-on:click="toggle">
  ...
</div>
```

La fonction `toggle` est reconnue dans le DOM car elle est définie dans le composant `dropdown`. Une notion importante de contexte est ici présente :
```javascript
Alpine.data('dropdown', () => ({
  open: false,
 
  toggle() { 
    this.open = ! this.open
  }
}))
```

---
# La directive x-text

Il est également possible de gérer de la donnée
```html
<h1 x-data="component1" x-text="welcome"></h1>
```

<br/>

Si la variable existe dans le contexte `data` de `component1`, le texte sera alors directement affiché au sein de la balise `h1` :

```javascript
Alpine.data('component1', () => ({
  welcome: "Welcome to this introduction"
}))
```
<br/>
<br/>
<br/>

---

# La directive x-init

Il est également possible d'intiliaser une variable depuis `x-data` :
```html
<h1 x-data="{welcome: 'Welcome to this tutorial' }'" x-text="welcome"></h1>
```

Dans ce cas, on peut s'abstenir de la création d'un composant. Toutefois, il est également possible de passer par la directive `x-init` :

```html
<h1 x-data="component1" x-init="welcome = 'Welcome!'" x-text="welcome"></h1>
```

La variable n'était pas forcément initialisée lors de l'enregistrement du composant :

```javascript
Alpine.data('component1', () => ({
  welcome: undefined
}))
```

---

# La directive x-init

Ce n'est pas la manière la plus propre pour maintenir son code :

```html
<h1 x-data="component1" x-text="welcome"></h1>
```

Une manière assez propre d'initialiser un composant, est de créer une fonction `init` qui est appelée par défaut (équivalent au `x-init` mais localisée côté JavaScript) :

```javascript
Alpine.data('component1', () => ({
  welcome: undefined,
  init() {
      this.welcome = "That's better there!"
  }
}))
```

---

# La directive x-show

Il est possible d'afficher ou non un élément du DOM en fonction de l'état d'une variable du composant :
```html
<div x-data="openComponent" x-on:click="toggle">
    <div x-show="open">
        ...
    </div>
    <div x-show="!open">
        ...
    </div>
</div>
```

**Important :** les données du composant d'un élément DOM parent sont accessibles depuis les enfants. Une notion de portée est donc présente.

---

# La directive x-bind

Par moment il peut-être nécessaire qu'un champs particulier soit associé à une variable de notre composant. `x-bind` est là pour vous :
```html
<div x-data="{ placeholder: 'Type here...' }">
  <input type="text" x-bind:placeholder="placeholder">
</div>
```

Un autre exemple, sur le choix de classe d'un élément du DOM :
```html
<div x-data="{ open: false }">
  <button x-on:click="open = ! open">Toggle stupid sentence</button>
 
  <div :class="open ? '' : 'hidden'">
    Hide or not to hide, that is the question
  </div>
</div>
```

---
# La directive x-if

Dans le même principe, il est possible d'utiliser `x-if` :
```html
<template x-data="component1" x-if="show">
  <h2 x-text="title"></h2>
</template>
```

Lui aussi se base sur un état de variable :
```javascript
Alpine.data('component1', () => ({
  show: false
  title: "My best title"
}))
```

**Note :** Pourquoi utiliser `x-if` et pas `x-show` ? Bonne question ! En réalité, `x-if` ne définit pas crée pas l'élément du DOM du tout. `x-show` lui, le cache uniquement.

---
# La directive x-for

Pour simplifier l'affichage, autant en profiter : 
```html
<template x-data="postComponent" x-for="post in posts">
  <h2 x-text="post.title"></h2>
</template>
```

Bien entendu, vos données doivent être initialisées :
```javascript
Alpine.data('postComponent', () => ({
  posts: ["post1", "post2", "post3"]
}))
```

**Note :** si besoin n'oubliez pas de créer une fonction `init`.

---
# La directive x-model

Il est également possible d'associer à des formulaires certaines variables :
```html
<form x-data="composant1">
    <input type="text" x-model="message">
    <span x-text="message">
    ...
</form>
```

La variable pouvant être traitée avant envoie d'informations :
```javascript
Alpine.data('postComponent', () => ({
  message: ""
  submitForm() {
      console.log(this.message);
  }
}))
```

---
# Alpine Store

Par moment il peut-être nécessaire que certaines variables soient accessibles par n'importe quel élément du DOM et donc non locales à un composant.

À cet effet, **Alpine** propose un `store` :

```javascript
    document.addEventListener('alpine:init', () => {
        Alpine.store('hideElement', {
            on: false
        })
    })
```

La variable est ensuite accessible depuis le DOM :
```html
<div x-data x-show="$store.hideElement.on">
    ...
</div>
```

---

# Alpine JS

C'est fini pour l'introduction ! Il existe bien d'autres choses ! N'hésitez pas à vous documenter sur les possibilités du framework : [https://alpinejs.dev/start-here](https://alpinejs.dev/start-here)