import * as THREE from 'three';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader'

import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader'
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js'

/**
 * Maze class
 * @param wrapper
 * @param button
 */


class ThreeMaze
{
    constructor(window, wrapper, width, height) {
        // Object attributes
        this.window = window
        this.wrapper = wrapper;
        this.camera = {};
        this.scene = {};
        this.materials = {};
        this.map = [];
        this.renderer = {};
        this.end = {};
        this.width = width;
        this.height = height;
        this.thickness = 30;   
        this.hider_H1 = undefined
        this.hider_H2 = undefined
        this.mixer = undefined
        this.clips = undefined // tableau de toutes les animations
        this.idle = undefined // animation idle
        this.action = undefined
        this.clock = new THREE.Clock(); // clock pour animation
    }


    generateEmptyMap = (width, height) => {
        var map = [];
    
        for (var x = 1; x <= width; x += 1)
        {
            map[x] = [];
            for (var y = 1; y <= height; y += 1)
            {
                map[x][y] = 0;  
            }
        }

        return map;
    }
    
    onGenerateMaze(map_representation, map_orientation, map_resistance, map_deplacement, map_interaction, map_player_name)
    {
        var new_map = this.generateEmptyMap(this.width, this.height);

        for (var x = this.width; x > 0; x -= 1)
        {
            for (var y = 1; y < this.height + 1; y += 1)
            {    

        
                /* --- MAP REPRESENTATION --- */
                // store current kind of cell
                new_map[x][y] = {
                    'kind': map_representation[x][y] // block or player
                }

                // check if necessary to update (need to be improved => some blocks can be damaged)
                if (typeof this.map[x] != 'undefined' && typeof this.map[x][y] != 'undefined' 
                    && new_map[x][y].kind === this.map[x][y].kind) {

                    new_map[x][y] = this.map[x][y];
                    continue;

                } else if (typeof this.map[x] != 'undefined' && typeof this.map[x][y] != 'undefined') {
                    
                    // check if player and take care in this case of the two mesh
                    if (this.map[x][y].player !== undefined) {
                        
                        this.map[x][y].player.visible = false;
                        this.map[x][y].mesh.visible = false;
                        this.scene.remove(this.map[x][y].player);
                        this.scene.remove(this.map[x][y].mesh);
                    }
                    else {
                        this.map[x][y].mesh.visible = false;
                        this.scene.remove(this.map[x][y].mesh);
                    }
                }

                // Adds a new mesh if needed
                if (map_representation[x][y] === "BORDER")
                {
                    // Generates the mesh for border
                    var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2, this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(wall_geometry, this.materials.grey);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                
                }
                else if (map_representation[x][y] === "GROUND") {
                     // generate the mesh with ground
                     var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);                    
                     new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                     new_map[x][y].mesh.visible = true;
                     new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                     this.scene.add(new_map[x][y].mesh);
                }
                else if (map_representation[x][y] === "HIDER")
                { 
                    let y_map = y
                    let x_map = x

                    const fbxLoader = new FBXLoader()
                    fbxLoader.load('./objet/players/hider.fbx', (objet) => {

                        //scene.add(objet.scene)  
                        let mesh_object = objet
                        mesh_object.visible = true
                        mesh_object.scale.set(0.08,0.08,0.08)
                        mesh_object.position.set(x_map * this.thickness - ((this.width * this.thickness) / 2), this.thickness / 2, y_map * this.thickness - ((this.height * this.thickness) / 2))
                        
                        // Checking orientation
                        if (map_orientation[x_map][y_map] === "Left") mesh_object.rotateY(-Math.PI);
                        else if (map_orientation[x_map][y_map] === "Right") mesh_object.rotateY(2*Math.PI);
                        else if (map_orientation[x_map][y_map] === "Top") mesh_object.rotateY(-Math.PI/2);
                        else if (map_orientation[x_map][y_map] === "Bottom") mesh_object.rotateY(Math.PI/2);
                        else {}
                        
                        new_map[x_map][y_map].player = mesh_object
                        this.scene.add(new_map[x_map][y_map].player);
                        
                        },(xhr) => {
                            //console.log((xhr.loaded / xhr.total) * 100 + '% loaded')
                        }, (error) => {
                            console.log(error)
                    })  


                    // also add the material mesh if players have one
                    if(map_deplacement[x][y] !="EMPTY"){
                        let mater = undefined
                        if (map_deplacement[x][y] === "WOOD"){
                            var ground = new THREE.BoxGeometry((this.thickness)/2, (this.thickness)/2, (this.thickness)/2, 1, 1, 1);                    
                            mater = new THREE.Mesh(ground, this.materials.wood);
                            mater.visible = true;
                            mater.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 80, y * this.thickness - ((this.height * this.thickness) / 2));
                        }
                        if (map_deplacement[x][y] === "STONE"){
                            var ground = new THREE.BoxGeometry((this.thickness)/2, (this.thickness)/2, (this.thickness)/2, 1, 1, 1);                   
                            mater = new THREE.Mesh(ground, this.materials.stone);
                            mater.visible = true;
                            mater.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 80, y * this.thickness - ((this.height * this.thickness) / 2));
                        }
                        if (map_deplacement[x][y] === "METAL"){
                            var ground = new THREE.BoxGeometry((this.thickness)/2, (this.thickness)/2, (this.thickness)/2, 1, 1, 1);                
                            mater = new THREE.Mesh(ground, this.materials.metal);
                            mater.visible = true;
                            mater.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 80, y * this.thickness - ((this.height * this.thickness) / 2));
                        }

                        // also add the ground mesh
                        var ground_mater = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);                    
                        let ground_mesh = new THREE.Mesh(ground_mater, this.materials.ground);
                        ground_mesh.visible = true;
                        ground_mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
    
                        const group = new THREE.Group();
                        group.add( mater );
                        group.add( ground_mesh );
                        new_map[x][y].mesh = group
                        this.scene.add(new_map[x][y].mesh);
                    }
                    else {
                        // also add the ground mesh
                        var ground_mater = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);                    
                        new_map[x][y].mesh = new THREE.Mesh(ground_mater, this.materials.ground);
                        new_map[x][y].mesh.visible = true;
                        new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                        this.scene.add(new_map[x][y].mesh);
                    }
                              
                } 

                else if (map_representation[x][y] === "SEEKER")
                {   
                   
                    let y_map = y
                    let x_map = x

                    const fbxLoader = new FBXLoader()
                    fbxLoader.load('./objet/players/seeker.fbx', (objet) => {

                        //scene.add(objet.scene)  
                        let mesh_object = objet
                        mesh_object.scale.set(0.08,0.08,0.08)
                        mesh_object.position.set(x_map * this.thickness - ((this.width * this.thickness) / 2), this.thickness / 2, y_map * this.thickness - ((this.height * this.thickness) / 2))

                            // Checking orientation
                        if (map_orientation[x_map][y_map] === "Left") mesh_object.rotateY(-Math.PI);
                        else if (map_orientation[x_map][y_map] === "Right") mesh_object.rotateY(2*Math.PI);
                        else if (map_orientation[x_map][y_map] === "Top") mesh_object.rotateY(-Math.PI/2);
                        else if (map_orientation[x_map][y_map] === "Bottom") mesh_object.rotateY(Math.PI/2);
                        else {}

                        new_map[x_map][y_map].player = mesh_object
                        
                        this.scene.add(new_map[x_map][y_map].player);
                        
                        },(xhr) => {
                            //console.log((xhr.loaded / xhr.total) * 100 + '% loaded')
                        }, (error) => {
                            console.log(error)
                      }) 

                       
                        // also add the ground mesh
                        var ground_mater = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);                    
                        new_map[x][y].mesh = new THREE.Mesh(ground_mater, this.materials.ground);
                        new_map[x][y].mesh.visible = true;
                        new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                        this.scene.add(new_map[x][y].mesh);
                      
                    
                } 
                else if (map_representation[x][y] === "STONE")
                {  
                    let mesh = undefined

                        if(map_resistance[x][y] <=100 && map_resistance[x][y] >=(100*2)/3){
                            // Generates the mesh for border
                            var wall_geometry = new THREE.BoxGeometry((this.thickness), (this.thickness * 2), (this.thickness), 1, 1, 1);
                            mesh = new THREE.Mesh(wall_geometry, this.materials.stone);
                            mesh.visible = true;
                            mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));                       
                        }

                        if(map_resistance[x][y] <(100*2)/3 && map_resistance[x][y] >=100/3){
                            // Generates the mesh for border
                            var wall_geometry = new THREE.BoxGeometry(((this.thickness)*2)/3, ((this.thickness * 2)*2)/3, ((this.thickness)*2)/3, 1, 1, 1);
                            mesh = new THREE.Mesh(wall_geometry, this.materials.stone);
                            mesh.visible = true;
                            mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));                        
                        }
                        if(map_resistance[x][y] <100/3 && map_resistance[x][y] >0){
                            // Generates the mesh for border
                            var wall_geometry = new THREE.BoxGeometry((this.thickness)/3, (this.thickness * 2)/3, (this.thickness)/3, 1, 1, 1);
                            mesh = new THREE.Mesh(wall_geometry, this.materials.stone);
                            mesh.visible = true;
                            mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));                   
                        }

                        new_map[x][y].mesh = mesh
                        this.scene.add(new_map[x][y].mesh);  
                        
                
                } 

                else if (map_representation[x][y] === "METAL")
                { 
                    let mesh = undefined

                         if(map_resistance[x][y] <=100 && map_resistance[x][y] >=(100*2)/3){
                            // Generates the mesh for border
                            var wall_geometry = new THREE.BoxGeometry((this.thickness), (this.thickness * 2), (this.thickness), 1, 1, 1);
                            mesh = new THREE.Mesh(wall_geometry, this.materials.metal);
                            mesh.visible = true;
                            mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));                        
                        }

                        if(map_resistance[x][y] <(100*2)/3 && map_resistance[x][y] >=100/3){
                            // Generates the mesh for border
                            var wall_geometry = new THREE.BoxGeometry(((this.thickness)*2)/3, ((this.thickness * 2)*2)/3, ((this.thickness)*2)/3, 1, 1, 1);
                            mesh = new THREE.Mesh(wall_geometry, this.materials.metal);
                            mesh.visible = true;
                            mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));                       
                        }
                        if(map_resistance[x][y] <100/3 && map_resistance[x][y] >0){
                            // Generates the mesh for border
                            var wall_geometry = new THREE.BoxGeometry((this.thickness)/3, (this.thickness * 2)/3, (this.thickness)/3, 1, 1, 1);
                            mesh = new THREE.Mesh(wall_geometry, this.materials.metal);
                            mesh.visible = true;
                            mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));                     
                        }

                        new_map[x][y].mesh = mesh
                        this.scene.add(new_map[x][y].mesh);  

                } 

                else if (map_representation[x][y] === "WOOD")
                { 
                    var wall_geometry = new THREE.BoxGeometry((this.thickness), (this.thickness * 2), (this.thickness), 1, 1, 1);
                        new_map[x][y].mesh = new THREE.Mesh(wall_geometry, this.materials.wood);
                        new_map[x][y].mesh.visible = true;
                        new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));                        
                    
                    this.scene.add(new_map[x][y].mesh);  
                
                } 

                else {
                    //by default other border
                    var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2, this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(wall_geometry, this.materials.grey);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                }

            }
        }

        this.map_representation = map_representation;
        this.map = new_map;
    };
 
    /**
     * Inits the scene
     */
    initScene()
    {
        // Scene
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0xffffff);

        // Materials
        const loader = new THREE.TextureLoader();

        const ground_material = new THREE.MeshStandardMaterial({
            map: loader.load('./objet/ground/Concrete_019_BaseColor.jpg'),
            normalMap: loader.load('./objet/ground/Concrete_019_Normal.jpg'), 
            displacementMap: loader.load('./objet/ground/Concrete_019_Height.png'), 
            displacementScale: 0.05,
            roughnessMap: loader.load('./objet/ground/Concrete_019_Roughness.jpg'), 
            roughness: 0.5,
        })

        const wall_material = new THREE.MeshStandardMaterial({
            map: loader.load('./objet/wall/Wall_Stone_022_basecolor.jpg'),
            normalMap: loader.load('./objet/wall/Wall_Stone_022_normal.jpg'), 
            displacementMap: loader.load('./objet/wall/Wall_Stone_022_height.png'), 
            displacementScale: 0.05,
            roughnessMap: loader.load('./objet/wall/Wall_Stone_022_roughness.jpg'), 
            roughness: 0.5,
            wireframe: false
        })

        const wood_material = new THREE.MeshStandardMaterial({
            map: loader.load('./objet/wood/Wood_Crate_001_basecolor.jpg'),
            normalMap: loader.load('./objet/wood/Wood_Crate_001_normal.jpg'), 
            displacementMap: loader.load('./objet/wood/Wood_Crate_001_height.png'), 
            displacementScale: 0.05,
            roughnessMap: loader.load('./objet/wood/Wood_Crate_001_roughness.jpg'), 
            roughness: 0.5,
            wireframe: false
        })

        const stone_material = new THREE.MeshStandardMaterial({
            map: loader.load('./objet/stone/Rock_044_BaseColor.jpg'),
            normalMap: loader.load('./objet/stone/Rock_044_Normal.jpg'), 
            displacementMap: loader.load('./objet/stone/Rock_044_Height.png'), 
            displacementScale: 0.05,
            roughnessMap: loader.load('./objet/stone/Rock_044_Roughness.jpg'), 
            roughness: 0.5,
            wireframe: false
        })

        const metal_material = new THREE.MeshStandardMaterial({
            map: loader.load('./objet/metal/metal.jpg'),
            wireframe: false
        })
        
        this.materials =
        {
            grey: wall_material,
            wood: wood_material,
            stone: stone_material,
            metal: metal_material,
            ground: ground_material,
            hider: new THREE.MeshLambertMaterial({color: 0x0066ff}),
            seeker: new THREE.MeshLambertMaterial({color: 0x990000}),
            orange: new THREE.MeshLambertMaterial({color: 0xbe842b}),
            dir: {
                
            }
        };

        // Camera
        this.camera = new THREE.PerspectiveCamera(45, 1, 1, 2000);
      /*  this.camera.position.x = 400;
        this.camera.position.y = 600;
        this.camera.position.z = -700;
*/
        this.camera.position.x = 1000;
        this.camera.position.y =800;
        this.camera.position.z = 200;

        this.camera.clicked = false;

        // Lights
        this.scene.add(new THREE.AmbientLight(0xc9c9c9));
        var directional = new THREE.DirectionalLight(0xc9c9c9, 0.5);
        directional.position.set(0, 0.5, 1);
        this.scene.add(directional);

        // this.camera.lookAt(this.scene.position);
        // Renderer
        this.renderer = typeof WebGLRenderingContext != 'undefined' && window.WebGLRenderingContext ? new THREE.WebGLRenderer({antialias: true}) : new THREE.CanvasRenderer({});
        this.wrapper.appendChild(this.renderer.domElement);
    };

    /**
     * Render loop
     * Sets the camera position and renders the scene
     */
    render()
    {
        requestAnimationFrame(this.render.bind(this));
        
        this.camera.lookAt(new THREE.Vector3(0, -150, 0));
        this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.render(this.scene, this.camera);
    };

    /**
     * Sets the scene dimensions on window resize
     */
    onWindowResize()
    {
        // Check if there is a better way to adjust width and height
        var width = (this.window.innerWidth / 12) * 8.5 || this.window.document.body.clientWidth;
        var height = this.window.innerHeight - 120 || this.window.document.body.clientHeight;
        this.renderer.setSize(width, height);
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
    };
}

export default ThreeMaze;