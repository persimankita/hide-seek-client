import ThreeMaze from './maze.js'
import Alpine from 'alpinejs'
import  Chart  from 'chart.js/auto';
import {SERVER_URL} from './params.js'

var maze = undefined;
var map_parent;
var old_map =  undefined;
var nombres_Blocs_cassés = 0;
var nombre_Blocs_déplacés =0;
var nombres_deplacements_H1 =0;
var nombres_deplacements_H2 =0;
var nombres_deplacements_S1 =0;
var nombres_deplacements_S2 =0;
var round_actuel =0;
var round_ancien =0;
var tab_dist_S1_H1  = [];
var tab_dist_S2_H1  = [];
var tab_dist_S1_H2 = [];
var tab_dist_S2_H2  = [];
var  myChart ;
var labels = [];
var hider_name;
var seeker_name;
var X_H1 =0;
var l ;
var Y_H1 =0 ;
var X_H2 =0;
var Y_H2 =0;
var X_S1 =0;
var Y_S1 = 0;
var X_S2 =0;
var Y_S2 =0;

var dist_S1_H1 =0 ;
var dist_S2_H1 =0;
var dist_S1_H2 = 0;
var dist_S2_H2 =0;



const wrapper = document.querySelector('.three');

const load = () => {
    
    let id = Alpine.store('gameId');

    fetch(`${SERVER_URL}/game?id=${id}`)
        .then(res => {

            // check for error response
            if (!res.ok) {
                // get error message from body or default to response status
                const error = response.status;
                return Promise.reject(error);
            }

            // remove info message (if exists)
            Alpine.store('gameInfo', "");
            
            return res.json();
        })
        .then(data => {
            
            // initialize game
            wrapper.innerHTML = ""
            Alpine.store('gameEnd', false);
            Alpine.store('gameRound', data["round"]);
            Alpine.store('gameMaxRounds', data["maxRounds"]);
            Alpine.store('gameHider', data["hider"]);
            Alpine.store('gameSeeker', data["seeker"]);


            let width = data['width']
            let height = data['height']

            let percentProgress = parseInt(parseFloat(data['round']) / parseFloat(data['maxRounds']) * 100.);
            
            Alpine.store('gameProgress', percentProgress);

            maze = new ThreeMaze(window, wrapper, width, height);
    
            // Inits
            maze.initScene();
            maze.onWindowResize();
            maze.render();


            //mise a zero des variables a la fin    
            
                
            
            var map = [];
            let map_orientation = [];
            let map_resistance = [];
            let map_deplacement = []
            let map_interaction = []
            let map_player_name= []
            let counter = 0;
            l =0;
            

            for (var x = 1; x <= width; x += 1)
            {
                map[x] = [];
                map_orientation[x] = [];
                map_resistance[x] = [];
                map_deplacement[x] = []
                map_interaction[x] = []
                map_player_name[x] = []

                for (var y = 1; y <= height; y += 1)
                {   
                    map_interaction[x][y] = "EMPTY"

                    if ('block' in data['cells'][counter]) {

                        if ('materialStr' in data['cells'][counter]["block"]){
                            map[x][y] = data['cells'][counter]["block"]["materialStr"]
                            map_orientation[x][y] = "EMPTY"
                            map_resistance[x][y] = data['cells'][counter]["block"]["resistance"]
                            map_deplacement[x][y] = "EMPTY"
                            map_player_name[x][y] = "EMPTY"
                        }
                        else {
                            map[x][y] = data['cells'][counter]["block"]["kindStr"]
                            map_orientation[x][y] = "EMPTY"
                            map_resistance[x][y] = data['cells'][counter]["block"]["resistance"]
                            map_deplacement[x][y] = "EMPTY"
                            map_player_name[x][y] = "EMPTY"
                        }
                    }
                    if ('player' in data['cells'][counter]) {
                        
                        map[x][y] = data['cells'][counter]["player"]["roleStr"]
                        map_orientation[x][y] = data['cells'][counter]["player"]["orientationStr"]
                        map_resistance[x][y] = "EMPTY"
                        map_player_name[x][y] = data['cells'][counter]["player"]["name"]
                        
                        if('block' in data['cells'][counter]["player"]){
                            map_deplacement[x][y] = data['cells'][counter]["player"]["block"]["materialStr"]
                        }
                    }
                    counter++;
                }
            }
            
            // load maze map
            old_map = map
            round_ancien =0;
            nombre_Blocs_déplacés =0;
            nombres_Blocs_cassés =0;
            nombres_deplacements_H1 =0;
            nombres_deplacements_H2 =0;
            nombres_deplacements_S1 = 0;
            nombres_deplacements_S2 = 0;
                
            round_actuel =0;
            round_ancien =0;
            tab_dist_S1_H1  = [];
            tab_dist_S2_H1  = [];
            tab_dist_S1_H2 = [];
            tab_dist_S2_H2  = [];
                  
            labels = [];
            X_H1 =0;
            l  = "";
            Y_H1 =0 ;
            X_H2 =0;
            Y_H2 =0;
            X_S1 =0;
            Y_S1 = 0;
            X_S2 =0;
            Y_S2 =0;  
                 
            Alpine.store('map_1', map);
           
            maze.onGenerateMaze(map, 
                map_orientation, 
                map_resistance, 
                map_deplacement, 
                map_interaction, 
                map_player_name);

            window.addEventListener('resize', maze.onWindowResize.bind(maze));
        }).catch(e => {
            console.log(e)
            Alpine.store('gameInfo', "Game not found...");
            Alpine.store('gameError', true);
        });
}

const updateLoop = () => {
    
    let id = Alpine.store('gameId');

    fetch(`${SERVER_URL}/game/step`, {
            method: 'POST', 
            body: JSON.stringify({"id": id})
        })
        .then(res => {

            // check for error response
            if (!res.ok) {
                // get error message from body or default to response status
                const error = res.status;
                return Promise.reject(error);
            }

            // remove error message (if exists)
            Alpine.store('gameInfo', "");
            
            return res.json();
        })
        
        .then(data => {
            
            // no server error currently
            Alpine.store('gameError', false);
            Alpine.store('gameRound', data["round"]);
            Alpine.store('gameMaxRounds', data["maxRounds"]);
            Alpine.store('gameHider', data["hider"]);
            Alpine.store('gameSeeker', data["seeker"]);

            let width = data['width']
            let height = data['height']
            console.log(data['round'], 'of', data['maxRounds'], '=> end? : ', data['end']);
            let gameEnd = data['end'];

            Alpine.store('gameEnd', gameEnd);

            if (gameEnd) {
                // clearInterval(canvasInterval);
                let currentWinner = data['winnerStr'];
                Alpine.store('gameInfo',`Game ${Alpine.store("gameId")} is finished! Winner is: ${currentWinner}`)
                Alpine.store('gameWinner', currentWinner);
            }

            let percentProgress = parseInt(parseFloat(data['round']) / parseFloat(data['maxRounds']) * 100.);

            Alpine.store('gameProgress', percentProgress);

            //création des variables
            
            var game_players = data["players"];
            var Player_Hider = 0;
            var Player_Seeker = 0;
            var Hider_Trouvés = 0;
            
            
            round_actuel = data["round"];

            
            for(var i=0 ; i< game_players.length; i++){
            if(game_players[i].roleStr == "HIDER"){
            Player_Hider += 1;
            }else if(game_players[i].roleStr == "SEEKER"){
            Player_Seeker += 1;
            }
            }
            
            //le nombre de hiders capturé
            for(var i=0; i< game_players.length; i++){
            if(game_players[i].roleStr == "HIDER" && game_players[i].captured == true){
            Hider_Trouvés += 1;
              }
            }

            
            //passa des valeurs à Alpine
            
            Alpine.store('PH', Player_Hider);
            Alpine.store('PS',Player_Seeker);
            
            
            //recuperation des donnés du joueur actuel
            
            var map = [];
            let map_orientation = []
            let map_resistance = []
            let map_deplacement = []
            let map_interaction = []
            let map_player_name = []
            let counter = 0;
            var current_Player = data["currentPlayer"];
            var current_Action = data["currentAction"];
            hider_name = data["hider"];
            seeker_name = data["seeker"];
            
            
            for (var x = 1; x <= width; x += 1)
            {
                map[x] = [];
                map_orientation[x] = []
                map_resistance[x] = []
                map_deplacement[x] = []
                map_interaction[x] = []
                map_player_name[x] = []

                for (var y = 1; y <= height; y += 1)
                {   
                    map_interaction[x][y] = data["currentAction"]["interaction"]

                    if ('block' in data['cells'][counter]) {

                        if ('materialStr' in data['cells'][counter]["block"]){
                            map[x][y] = data['cells'][counter]["block"]["materialStr"]
                            map_orientation[x][y] = "EMPTY"
                            map_resistance[x][y] = data['cells'][counter]["block"]["resistance"]
                            map_deplacement[x][y] = "EMPTY"
                            map_player_name[x][y] = "EMPTY"
                        }
                        else {
                            map[x][y] = data['cells'][counter]["block"]["kindStr"]
                            map_orientation[x][y] = "EMPTY"
                            map_resistance[x][y] = data['cells'][counter]["block"]["resistance"]
                            map_deplacement[x][y] = "EMPTY"
                            map_player_name[x][y] = "EMPTY"
                        }
                    }
                    if ('player' in data['cells'][counter]) {
                        
                        map[x][y] = data['cells'][counter]["player"]["roleStr"]
                        map_orientation[x][y] = data['cells'][counter]["player"]["orientationStr"]
                        map_resistance[x][y] = "EMPTY"
                        map_player_name[x][y] = data['cells'][counter]["player"]["name"]
                        
                        if('block' in data['cells'][counter]["player"]){
                            map_deplacement[x][y] = data['cells'][counter]["player"]["block"]["materialStr"]
                        }
                    }
                    counter++;
                }
             
            }



           
        //boucle pour connaitre les blocs deplacés ou cassés
        
        for (var x = 1; x <= width; x += 1){
            for (var y = 1; y <= height; y += 1){
                if(old_map[x][y] !== map[x][y]){
                    if(current_Player.roleStr === "HIDER" && current_Action.interaction === 2  ){
                           nombre_Blocs_déplacés += 1;
                    } else if ( current_Player.roleStr === "SEEKER" && current_Action.interaction === 1){
                        nombres_Blocs_cassés += 1;
                    }

                }
            }
        }
       /// boucle pour connaitre le nombre de deplacement
   
        for (var x = 1; x <= width; x += 1){
            for (var y = 1; y <= height; y += 1){
                if(old_map[x][y] !== map[x][y]){
                    if(current_Player.name === "H1" && current_Action.interaction === 0  ){
                           nombres_deplacements_H1 +=0.5;
                    } else if ( current_Player.name === "H2" && current_Action.interaction === 0){
                           nombres_deplacements_H2 +=0.5 ;
                    }else if ( current_Player.name === "S1" && current_Action.interaction === 0){
                           nombres_deplacements_S1 +=0.5 ;
                    }else if ( current_Player.name === "S2" && current_Action.interaction === 0){
                           nombres_deplacements_S2 +=0.5;
                    }

                }
            }
        }

      
        
        // passage de la map actuelle à l'ancienne map
        old_map = map; 

        //recuperer les données pour chaques courbes
        
        for(var i=0; i< game_players.length; i++){
            if(game_players[i].name ==="H1"){
                X_H1 = game_players[i].location.x;
                Y_H1 = game_players[i].location.y;
            }else if(game_players[i].name ==="H2"){
                X_H2 = game_players[i].location.x;
                Y_H2 = game_players[i].location.y;
            }else if(game_players[i].name ==="S1"){
                X_S1 = game_players[i].location.x;
                Y_S1 = game_players[i].location.y;
            }else if(game_players[i].name ==="S2"){
                X_S2 = game_players[i].location.x;
                Y_S2 = game_players[i].location.y;
            }
        }


        

        if( round_ancien != round_actuel  ){
            //calcule de la distance de la 1ère courbe : S1 par rapport a H1

         dist_S1_H1 = Math.sqrt((Math.pow(X_H1,2)-Math.pow(X_S1,2))+ (Math.pow(Y_H1,2)-Math.pow(Y_S1,2)));

        //calcule de la distance de la 2ème courbe : S2 par rapport a H1

        dist_S2_H1 = Math.sqrt((Math.pow(X_H1,2)-Math.pow(X_S2,2))+ (Math.pow(Y_H1,2)-Math.pow(Y_S2,2)));

        //calcule de la distance de la 3ème courbe : S1 par rapport a H2
 
        dist_S1_H2 = Math.sqrt((Math.pow(X_H2,2)-Math.pow(X_S1,2))+ (Math.pow(Y_H2,2)-Math.pow(Y_S1,2)));

        //calcule de la distance de la 4ème courbe : S2 par rapport a H2
     
        dist_S2_H2 = Math.sqrt((Math.pow(X_H2,2)-Math.pow(X_S2,2))+ (Math.pow(Y_H2,2)-Math.pow(Y_S2,2)));
        
        l = round_ancien.toString();
        labels.push(l);

        tab_dist_S1_H1.push(dist_S1_H1);
        tab_dist_S1_H2.push(dist_S1_H2);
        tab_dist_S2_H1.push(dist_S2_H1);
        tab_dist_S2_H2.push(dist_S2_H2);
        round_ancien =round_actuel;

        
        }
        
        //gestion de ma mini map
        
        //creation du labyrinthe
        map_parent = document.getElementById("map");

        map_parent.classList.add("d-flex");
        map_parent.classList.add("flex-column");
        map_parent.innerHTML ="";

         for(var x =1 ; x<=  width; x++){
            var div_parent = document.createElement("div");
            map_parent.appendChild(div_parent);
            div_parent.classList.add("d-flex");
            div_parent.classList.add("flex-row");
            div_parent.classList.add("parent-div");
            for(var y = 1; y<= height ; y++){
              var div_enfant = document.createElement("div");
             div_parent.appendChild(div_enfant);  
             div_enfant.classList.add("enfant-div");
             if(map[x][y]=== "BORDER"){
                   div_enfant.style.background = "brown"
            }else if(map[x][y] === "HIDER"){
                 div_enfant.style.background = "blue"

            }else if(map[x][y] === "SEEKER"){
                div_enfant.style.background = "red"

            }else if(map[x][y] === "WOOD"){
                div_enfant.style.background = "gold"

            }else if(map[x][y] === "STONE"){
                div_enfant.style.background = "grey"

           }else if(map[x][y] === "METAL"){
            div_enfant.style.background = "silver"

           }else if(map[x][y] === "GROUND"){
            div_enfant.style.background = "white"

           }
          }
        }

          
        
         ///tracage de graphe 
         

         var  ctx = document.getElementById('myChart').getContext('2d');
        
                 
                
                var data1 = {
                    labels: labels,
                    datasets: [
                      {
                        label: 'Dist_S1_H1',
                        data: tab_dist_S1_H1,
                        borderColor: '#4dc9f6',
                        backgroundColor: '#4dc9f6',
                      },
                      {
                        label: 'Dist_S2_H2',
                        data: tab_dist_S2_H2,
                        borderColor: '#f67019',
                        backgroundColor: '#f67019',
                      },
                      {
                        label: 'Dist_S1_H2',
                        data: tab_dist_S1_H2,
                        borderColor: '#f53794',
                        backgroundColor: '#f53794',
                      },

                      {
                        label: 'Dist_S2_H1',
                        data: tab_dist_S2_H1,
                        borderColor: '#537bc4',
                        backgroundColor: '#537bc4',
                      },

                    ]
                };

                if( typeof  myChart!=='undefined'){
                     myChart.destroy();
                }
                

                myChart = new Chart(ctx, {
                    
                    type: 'line',
                    data: data1,
                    options: {
                      responsive: true,
                      plugins: {
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Courbe statistique des distances'
                        }
                      }
                    },
                });
    
                
         
        //remise à zero des abcisse et ordonné ainsi que les distance et le round 
        
      
        // passage des valeurs à la store d'alpine pour pouvoir la récupérer dans l'html
        Alpine.store('n_casse', nombres_Blocs_cassés);
        Alpine.store('n_deplace', nombre_Blocs_déplacés);
        Alpine.store('hider_Trouve', Hider_Trouvés);
        Alpine.store('n_steps_H1', nombres_deplacements_H1);
        Alpine.store('n_steps_H2', nombres_deplacements_H2);
        Alpine.store('n_steps_S1', nombres_deplacements_S1);
        Alpine.store('n_steps_S2', nombres_deplacements_S2);
        Alpine.store('name_H', hider_name);
        Alpine.store('name_S', seeker_name);
      
        
        
        
  
        // reload maze map
            if (maze !== undefined) {
                maze.onGenerateMaze(map, map_orientation, 
                    map_resistance,
                    map_deplacement, 
                    map_interaction, 
                    map_player_name);
            }
        })
        .catch((e) => {
            console.log(e)

            Alpine.store('gameInfo', "Server cannot be reached");
            Alpine.store('gameError', true);
        });
}

export {load, updateLoop};

